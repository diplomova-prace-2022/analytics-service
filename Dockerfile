FROM openjdk:17-alpine
RUN addgroup -S my_group && adduser -S my_user -G my_group
USER my_user
COPY target/analytics-service*.jar /app/analytics-service.jar
COPY jobsSpecification /app/jobsSpecification
ENTRYPOINT ["java", "-jar", "/app/analytics-service.jar"]
