package com.example.analyticsservice.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {
    private static final String ANALYSIS_EXCHANGE_NAME = "analysis";
    private static final String ANALYSIS_LINKS_QUEUE_NAME = "link-analysis";

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean(name = "analysisExchange")
    public TopicExchange analysisExchange(){
        return new TopicExchange(ANALYSIS_EXCHANGE_NAME);
    }

    @Bean(name = "analysisLinksQueue")
    public Queue analysisLinksQueue(){
        return new Queue(ANALYSIS_LINKS_QUEUE_NAME);
    }

}
