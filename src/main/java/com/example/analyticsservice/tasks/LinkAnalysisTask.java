package com.example.analyticsservice.tasks;

import com.example.analyticsservice.model.spark.V1beta2SparkApplication;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.CustomObjectsApi;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Yaml;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class LinkAnalysisTask {

    private static final String GROUP = "sparkoperator.k8s.io";
    private static final String VERSION = "v1beta2";
    private static final String NAMESPACE = "spark-jobs";
    private static final String PLURAL = "sparkapplications";
    private static final String NAME = "pyspark-pi";

    @Value("${jobs.spec.location}")
    private String jobsSpecLocation;

    private final Map<String, LocalDateTime> jobTimeMap = new ConcurrentHashMap<>();

    @RabbitListener(queues = "#{analysisLinksQueue.name}")
    public void analyseLinks(String message) throws IOException {
        if(jobTimeMap.containsKey(NAME)
                && ChronoUnit.HOURS.between(jobTimeMap.get(NAME), LocalDateTime.now()) < 12) return;
        jobTimeMap.put(NAME, LocalDateTime.now());

        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);

        CustomObjectsApi api = new CustomObjectsApi();
        V1beta2SparkApplication jobSpec = Yaml.loadAs(new File(jobsSpecLocation + "/pyspark-pi.yml"), V1beta2SparkApplication.class);

        try {
            // Check if resource exists
            api.getNamespacedCustomObject(GROUP, VERSION, NAMESPACE, PLURAL, NAME);
            // Delete resource
            api.deleteNamespacedCustomObject(GROUP, VERSION, NAMESPACE, PLURAL, NAME, null, null, null, null, null);
        } catch (ApiException ex){
            log.info(ex.getResponseBody());
        }

        try {
            // Submit new spark job
            api.createNamespacedCustomObject(GROUP, VERSION, NAMESPACE, PLURAL, jobSpec, null, null, null);
        } catch (ApiException ex){
            log.error(ex.getResponseBody());
        }
    }
}
