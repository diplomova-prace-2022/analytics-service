from datetime import datetime

from pyspark.sql import functions as F, SparkSession

if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("BacklinksAnalysisPython") \
        .config("spark.cassandra.connection.host", "cassandra.link-service.svc.cluster.local") \
        .config("spark.cassandra.connection.port", "9042") \
        .config("spark.cassandra.auth.username", "cassandra") \
        .config("spark.cassandra.auth.password", "cassandraAdmin") \
        .getOrCreate()

    today = datetime.now().strftime("%Y-%m-%d")

    # Load links gathered today from Apache Cassandra
    links =  spark.read.format("org.apache.spark.sql.cassandra") \
        .options(table="links", keyspace="links") \
        .load()
    links = links.filter(F.col("date") == F.lit(today))

    # Count how many links go to every domain
    links_agg = links.groupBy("target").count()\
        .toDF("domain", "incoming_links_count")\
        .withColumn("date", F.lit(today))

    # Save data to Apache Cassandra
    links_agg.write.format("org.apache.spark.sql.cassandra")\
        .options(table="link_domain_aggregation", keyspace="links")\
        .mode("append")\
        .save()

    spark.stop()